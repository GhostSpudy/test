<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="SHORTCUT ICON" href="https://scandiweb.com/assets/images/scandiweb_logo.png" type="image/x-icon">
        <link rel="stylesheet" href="css/styles.css">
        <title>Product List</title>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </head>

    <body>
        <header>
            <b>Product Add</b>
            <div class="button">
                <button form="data" name="save" type="submit">Save</button>
                <button form="data" onclick="document.location.href='index.php';" type="reset">Cancel</button>
            </div>
        </header>

        <form method="POST" id="data" class="bodyAdd">

            <?php
                if (isset($_POST["save"])) {
                    require_once "classes/AddProduct.php";
                    
                    $add = new \classes\AddProduct($_POST);

                    $add->addProductToDb();
                }
            ?>

            <span class="error">Please, provide the data of indicated type<br></span>
            <label for="sku">SKU</label>
            <input type="text" name="sku" id="sku"><br>
            <label for="name">Name</label>
            <input type="text" name="name" id="name"><br>
            <label for="price">Price ($)</label>
            <input type="number" min="0" step="0.01" name="price" id="price"><br>
            <label for="types">Type Switcher</label>
            <select name="types" id="types">
                <option disabled selected>Type Switcher</option>
                <option value="dvd">DVD</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select><br>
            
            <div class="dvd">
                <label for="size">Size (MB)</label>
                <input type="number" min="0" step="0.01" name="size" id="size"><br>
                <p>"Please, provide size"</p>
            </div>

            <div class="book">
                <label for="weight">Weight (KG)</label>
                <input type="number" min="0" step="0.01" name="weight" id="weight"><br>
                <p>"Please, provide weight"</p>
            </div>

            <div class="furniture">
                <label for="height">Height (CM)</label>
                <input type="number" min="0" step="0.01" name="height" id="height"><br>
                <label for="width">Width (CM)</label>
                <input type="number" min="0" step="0.01" name="width" id="width"><br>
                <label for="length">Length (CM)</label>
                <input type="number" min="0" step="0.01" name="length" id="length"><br>
                <p>"Please, provide dimensions"</p>
            </div>
        </form>

        <footer>
            Scandiweb Test assignment
        </footer>
    </body>
</html>