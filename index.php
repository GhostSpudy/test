<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="SHORTCUT ICON" href="https://scandiweb.com/assets/images/scandiweb_logo.png" type="image/x-icon">
        <link rel="stylesheet" href="css/styles.css">
        <title>Product List</title>
    </head>

    <body>
        <header>
            <b>Product List</b>
            <div class="button">
                <button onclick="document.location.href = 'add.php';">ADD</button>
                <button form="products" name="delete" type="submit">MASS DELETE</button>
            </div>
        </header>

        <main>
            <form method="post" id="products" class="bodyShow">

                <?php 
                    require_once "classes/ProductManagement.php";

                    $show = new \classes\ProductManagement();

                    $show->showAllProduct();

                    if (isset($_POST["delete"])) {
                        $delete = new \classes\ProductManagement();

                        $delete->deleteProducts($_POST["delete"]);
                    }
                ?>

            </form>
        </main>

        <footer>
            Scandiweb Test assignment
        </footer>
    </body>
</html>