<?php

namespace classes;

require_once "Types.php";

class Product {

    protected $sku;
    protected $name;
    protected $price;
    protected $type;
    protected $description;

    public function __construct($array = array()) {
        $this->sku = $array["sku"];
        $this->name = $array["name"];
        $this->price = $array["price"];
        $this->type = $array["types"];
    
        $typeSelection = new \classes\TypeSelection();
        $this->description = $typeSelection->getTypeDescriptionForAdd($this->type, $array);
    }

}

?>