<?php

namespace classes;

use classes\Product;
use classes\DB;

require_once "Product.php";
require_once "Db.php";

class AddProduct extends Product {

    private function checkSku() {
        $db = new DB();

        $skuQuery = $db->query("SELECT * FROM `products` WHERE `sku` = '$this->sku'");
        
        if (!$skuQuery) {
            return true;
        }

        return false;
    }

    public function addProductToDb() {
        if ($this->checkSku()) {
            $db = new DB();

            $db->execute("INSERT INTO `products` (`sku`, `name`, `price`, `type`, `description`) 
                        VALUES ('$this->sku', '$this->name', '$this->price', '$this->type', '$this->description')");

            $db->closeConnect();

            header('Location: index.php');
        } else {
            echo "<span><font color='red'>This SKU already exists</font></span><br>";
        }
    }

}

?>