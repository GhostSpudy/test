<?php

namespace classes;

class TypeSelection {
    private $arrayTypes = array();

    public function __construct() {
        $this->arrayTypes["dvd"] = new \classes\Dvd();
        $this->arrayTypes["book"] = new \classes\Book();
        $this->arrayTypes["furniture"] = new \classes\Furniture();
    }

    public function getTypeDescriptionForAdd($type, $array = array()) {
        return $this->arrayTypes["$type"]->descriptionForAdd($array);
    }

    public function getTypeDescriptionForShow($type, $description) {
        return $this->arrayTypes["$type"]->descriptionForShow($description);
    }

}

abstract class Type {
    abstract function descriptionForAdd($array = array());

    abstract function descriptionForShow($description);
}

class Dvd extends Type{
    public function descriptionForAdd($array = array()) {
        return $array["size"];
    }

    public function descriptionForShow($description) {
        return "Size: " . $description . " MB";
    }
}

class Book extends \classes\Type{
    public function descriptionForAdd($array = array()) {
        return $array["weight"];
    }

    public function descriptionForShow($description) {
        return "Weight: " . $description . " KG";
    }
}

class Furniture extends Type{
    public function descriptionForAdd($array = array()) {
        return $array["height"] . "x" . $array["width"] . "x" . $array["length"];
    }

    public function descriptionForShow($description) {
        return "Dimension: " . $description;
    }
}

?>