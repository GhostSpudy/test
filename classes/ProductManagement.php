<?php

namespace classes;

use classes\DB;

require_once "Db.php";
require_once "Types.php";

class ProductManagement extends DB {

    private $arr = array();
    private $toDelete = array();
    private $selectAll = array();

    public function deleteProducts($arr = array()) {
        $this->arr = $arr;

        for ($i = 0; $i < count($this->arr); $i++) {
            $this->toDelete[] = $this->arr[$i];
        }

        parent::execute("DELETE FROM `products` WHERE `id` IN (".implode($this->toDelete, ', ').")");

        parent::closeConnect();

        //header("Refresh:0");
        header("Location: .");
    }

    public function showAllProduct() {
        $this->selectAll = parent::query("SELECT * FROM `products`");

        $typeSelection = new \classes\TypeSelection();

        for ($i = 0; $i < count($this->selectAll); $i++) {
            $id = $this->selectAll[$i]["id"];
            $sku = $this->selectAll[$i]["sku"];
            $name = $this->selectAll[$i]["name"];
            $price = $this->selectAll[$i]["price"];
            $type = $this->selectAll[$i]["type"];
            $description = $this->selectAll[$i]["description"];

            $descript = $typeSelection->getTypeDescriptionForShow($type, $description);

            echo '<div>';
            echo '<input type="checkbox" id="'.$id.'" name="delete[]" value="'.$id.'"><br>';
            echo '<div>';
            echo '<label for="'.$id.'">'
                    .$sku.'<br>'
                    .$name.'<br>'
                    .$price. " $" .'<br>'
                    .$descript.'
                </label><br>';
            echo '</div>';
            echo '</div>';
        }
    }

}

?>