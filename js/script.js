$(document).ready(function() {
    var skuCheck = /^[0-9A-Za-z]+$/;
    var nameCheck = /^[A-Za-z\s]+$/;

    //Changes the additional description of the product regarding its type
    $('select').change(function() {
        var a = $(this).find('option:selected');
        var b = a.val();
        $('#size, #weight, #height, #width, #length').val('');
        $('.dvd, .book, .furniture').hide();
        $('.' + b).show();
    });

    //Checks if the information is correct
    $('input').on('input', function() {
        var query = $(this).attr('name');
        if (query == "sku") {
            if(skuCheck.test($(this).val())) {
                $('.error').hide();
            } else {
                $('.error').show();
            }
        }

        if (query == "name") {
            if(nameCheck.test($(this).val())) {
                $('.error').hide();
            } else {
                $('.error').show();
            }
        }
    });

    //Checks for empty fields and correctness of information when saving the item
    $('form').submit(function() {
        var error = 0, error2 = 0;
        $('form').find(':input').each(function() {
            var queryName = $(this).attr("name");
            if (queryName == "sku") {
                if(!$(this).val()) {
                    error = 1;
                } else if (!skuCheck.test($(this).val())) {
                    error2 = 1;
                }
            }

            if (queryName == "name") {
                if(!$(this).val()) {
                    error = 1;
                } else if (!nameCheck.test($(this).val())) {
                    error2 = 1;
                }
            }

            if (queryName == "price") {
                if(!$(this).val()) {
                    error = 1;
                }
            }

            if ($('.dvd').is(':visible')) {
                if (!$('#size').val()) {
                    error = 1;
                }
            } else if ($('.book').is(':visible')) {
                if (!$('#weight').val()) {
                    error = 1;
                }
            } else if ($('.furniture').is(':visible')) {
                if (!$('#height').val() || !$('#width').val() || !$('#length').val()) {
                    error = 1;
                }
            } else {
                rror = 1;
            }
        });

        if (error == 1) {
            alert("Please, submit required data");
            return false;
        } else if (error2 == 1) {
            alert("Please, provide the data of indicated type");
            return false;
        }
    });
});